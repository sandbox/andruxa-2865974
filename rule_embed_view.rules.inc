<?php

/**
 * @file
 * Rules integration file.
 */

/**
 * Implements hook_rules_data_info().
 */
function rule_embed_view_rules_data_info() {
  return array(
    'raw_render' => array(
      'label' => t('raw html'),
      'token type' => 'rule_embed_view_raw_render',
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function rule_embed_view_rules_action_info() {
  $actions = array();
  $actions['rule_embed_view'] = array(
    'label' => t('Get rendered view HTML'),
    'parameter' => array(
      'view_display' => array(
        'type' => 'text',
        'label' => t('Select view'),
        'options list' => '_rule_embed_view_options_list',
        'default mode' => 'input',
      ),
      'view_args' => array(
        'type' => 'list<text>',
        'label' => t('View arguments'),
        'optional' => TRUE,
        'allow null' => TRUE,
      ),
    ),
    'group' => t('Views'),
    'provides' => array(
      'rendered_view' => array(
        'type' => 'raw_render',
        'label' => t('Rendered view'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'rule_embed_view_render',
    ),
  );
  return $actions;
}

/**
 * Callback for the rules action
 */
function rule_embed_view_render($view_display, $view_args = array()) {
  list($view_id, $display_id) = explode('|', $view_display);
  $view = views_get_view($view_id);
  $view->set_display($display_id);
  $view->set_arguments($view_args);
  $view->execute();
  return array(
    'rendered_view' => $view->render(),
  );
}

/**
 * Options list callback returning a list of view_id|display_id
 */
function _rule_embed_view_options_list() {
  $options = array();
  $views = views_get_all_views();
  $views_ids = array();
  foreach ($views as $view_id => $view) {
    if (!$view->disabled) {
      $views_ids[$view_id] = empty($view->human_name) ? $view_id : $view->human_name;
    }
  }
  asort($views_ids);
  foreach ($views_ids as $view_id => $view_name) {
    $view = $views[$view_id];
    foreach ($view->display as $display_id => $display) {
      $options[$view_name][$view_id . '|' . $display_id] = $display->display_title;
    }
  }
  return $options;
}
